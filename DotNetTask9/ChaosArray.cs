﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace DotNetTask9
{
    public class ChaosArray<T>
    {
        private  T [] _chaosarray; // Making an object class, so it should be able to take any value

        public ChaosArray() {
            _chaosarray = new T[100]; //100 is default size
        } //Default constructor

        public ChaosArray(int size) //If user want to declare the size of the Array
        {
            _chaosarray = new T[size];
        }
        public void Add(Object value) // Add method that insert randomly to an array and thrown an own error if needed
        {
            Random rm = new Random();
            int index = rm.Next(_chaosarray.Length);
            if (_chaosarray[index] != null)
            {
                Console.WriteLine($"Crashed a index {index}");
                throw new UnavailableSpaceException(); // Throw cusom exception
            }
            else {
                Console.WriteLine($"{value} is added at {index}"); //Added value at random index
                _chaosarray[index] = (T)value;
            }
        }
        public Object retrieveValue()// Function that returns an object
        {
            Random rm = new Random(); //Inserting at random location
            int variable = rm.Next(_chaosarray.Length);// Length is never more than the length of the chaosArray 
            if (_chaosarray[variable] == null)
            {
                throw new UnavailableSpaceException(); // Throw custom exception for unavailable space 
            }
            else
            {
                return $"Found the value{_chaosarray[variable]} at ${variable}"; //Write the value and where it were found
            }

        }
    }

}

