﻿using System;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

namespace DotNetTask9
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<Object> chaosArray=new ChaosArray<object>(100); //Makes an array of size 1000
            try
            {
                for (int i = 0; i < 100; i++)
                {
                    chaosArray.Add(i);//Adding values, the array should be able to take any value
                }
               
            }
            catch(UnavailableSpaceException) //Catches custom Exception
            {
                Console.Error.WriteLine("There were no more room in the array, please try again"); //Catching the custom array
            }
           

            /*################################Printing the values for the Array####################################*/
            
            try // 
            {
                for (int c = 0; c < 100; c++)
                {
                    Console.WriteLine(chaosArray.retrieveValue());//Calling the retrieve funtion, 100 is just used for test dataset
                }
            }
            catch (UnavailableSpaceException) //Cath
            {
                Console.Error.WriteLine("There are no values in that place!");
            }
            

        }   
    }
}
