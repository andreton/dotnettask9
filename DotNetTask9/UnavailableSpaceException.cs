﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetTask9
{
    class UnavailableSpaceException : Exception //custom exception derives from exception 
    {
        public UnavailableSpaceException() //Constructor
        { }

        public UnavailableSpaceException(string message) : base(message) { }//No more space exception

        public UnavailableSpaceException(string message, int place) : base(message)// Another custom exception if needed the place(index as well)
        {
        }

    }
}
